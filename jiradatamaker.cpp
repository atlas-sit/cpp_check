#include <iostream>
#include <sys/stat.h> 
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <array>
#include <memory>
//#include "eraselastchar.cpp"

using namespace std;

string jiraexecTag(const string cmd) { 
array <char, 128> buffer;
string result;
unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
	if (!pipe) {
	    throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
	    result += buffer.data();
	}
return result;

}	

const std::string jmDT() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d%m%Y", &tstruct);

    return buf;
}
const std::string jDT() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m-%Y", &tstruct);

    return buf;
}

string GetjiraData(string tag, string line){
	int size = tag.length()+2;
	int pos = line.find(tag+"=\"");
	string data = "";
	for( int i=pos+size; ; i++){

		if(line[i]== '=' && line[i+1]== '"')
			return "";
			
		if(line[i]== '"')
			return data;
			
		data += line[i];	
	}
	return "";
}

int main(){ //jiradatamaker
	
	chdir("athena");
	string athenafetch = jiraexecTag(" git fetch --tags");
	string tagsName = jiraexecTag("git describe --tags --abbrev=0");
	tagsName = tagsName.substr(0,tagsName.length()-1);
	chdir("..");
	
	ifstream read("results.xml");
	string line;
	auto divdate = jDT();
	auto id_date = jmDT();
	int counter = 1;
	string jsonhead = "{\"issueUpdates\":[";
	string jsonend = "]}";
	string jsondata = "";
	string href = "https://gitlab.cern.ch/atlas/athena/tree/";

	while(getline(read, line)){

		if(line.length()<=40)
			continue;
			
		if(line.find("<error") == string::npos)
			continue;
			
			string scan_date = GetjiraData("scan_date",line);
			
			string file = GetjiraData("file",line);
				file = file.substr(6,file.length()+1);
				
			string mrdate = GetjiraData("mrdate",line);
			
			string lineN = GetjiraData("line",line);
			
			string msg = GetjiraData("msg",line);
			
			string author = GetjiraData("author",line);
			
			string mail = GetjiraData("mail",line);

			string username = GetjiraData("username",line);


		jsondata += "{\"update\":{},\"fields\":{\"project\":{\"key\":\"ATLASSQ\"},";
		jsondata += "\"summary\": \"Cppcheck Scan Report:"+ scan_date+"\",";
		jsondata += "\"description\": \"||[Full Defect List Site|https://atlas-cppcheck.web.cern.ch/]||  || \\n ";
		jsondata += "||ID||" + id_date + to_string(counter++) + "| \\n ";
		jsondata += "||Scan Date||" + scan_date + "| \\n ";
		jsondata += "||Mr Date||" + mrdate + "| \\n ";
		jsondata += "||File||[athena"+ file +"|"+ href + tagsName + file +"]| \\n ";
		jsondata += "||Line||[" + lineN + "|" + href + tagsName + file + "#L" + lineN + "]| \\n ";
		jsondata += "||Defect Message||{color:red}" + msg + "{color}" + "| \\n ";
		jsondata += "||Author||"+ author + "| \\n ";
		jsondata += "||Mail||" + mail + "| \\n \",";
		jsondata += "\"issuetype\": {\"name\": \"Bug\" },";
		jsondata += "\"assignee\": {\"name\": \"" + username + "\"}}},";
		

}

	ofstream write("jj.txt");
	write << jsonhead + jsondata + jsonend;
	
	return 0;	
}
