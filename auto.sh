#!/bin/bash
chmod +x auto.sh

source config.sh

#Scanning timestamp
printf "\n[$TIMESTAMP] -- scan date" | tee -a $log_scandate

#pulls athena, starts cppcheck scanning, compares defect files, gets defects info
g++ -std=c++11 automate.cpp -o automate.out
./automate.out | tee $log_auto

#converts xml to html
./convertor.sh | tee -a $log_auto

#checks if defects exist. if yes creates jira tickets
g++ -std=c++11 check_defects.cpp -o check_defects.out
./check_defects.out | tee -a $log_auto


#PAUSE
