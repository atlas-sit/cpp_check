#include <iostream>
#include <sys/stat.h> 
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <array>
#include <memory>

using namespace std;

string execTag(const string cmd) { 
array <char, 128> buffer;
string result;
unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
	if (!pipe) {
	    throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
	    result += buffer.data();
	}
return result;

}	

const std::string currentDT() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d%m%Y", &tstruct);

    return buf;
}
const std::string cDT() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m-%Y", &tstruct);

    return buf;
}

string GetData(string tag, string line){
	int size = tag.length()+2;
	int pos = line.find(tag+"=\"");
	string data = "";
	for( int i=pos+size; ; i++){

		if(line[i]== '=' && line[i+1]== '"')
			return "";
			
		if(line[i]== '"')
			return data;
			
		data += line[i];	
	}
	return "";
}

int main(){ //convertor
	
	chdir("athena");
	string athenafetch = execTag(" git fetch --tags");
	string tagsName = execTag("git describe --tags --abbrev=0");
	tagsName = tagsName.substr(0,tagsName.length()-1);
	chdir("..");
	
	ifstream read("results.xml");
	string line;
	auto divdate = cDT();
	
	const char *header = 
		"<!DOCTYPE html><html><head><title>Cppcheck-List</title>"
		"<meta http-equiv=\"Cache-Control\" content=\"no-store\">"
		"<meta http-equiv=\"Pragma\" content=\"no-cache\">"
		"<meta http-equiv=\"Expires\" content=\"0\">"
		"<style>"
		"table {border: 2px dotted black; width: 100%; height: 100%;} "
		"th {border: 1px solid black; padding: 5px;} "
		"tr {min-height: 150px;}"
		".header {font-size: 17px; font-weight: 900;border: 0px; border-bottom: 10px solid black;} "
		".file {max-width: 400px; min-width: 150px; font-size: 13px; overflow-wrap: break-word;} "
		".status {background-color: lime; font-size: 13px; font-weight: 900;}"
		".id_num {font-size: 13px;font-weight: 900;}"
		".scan_date {font-size: 13px;font-weight: 900;}"
		".mrdate {font-size: 13px;font-weight: 900;}"
		".line {font-size: 13px;font-weight: 900;}"
		".msg {max-width: 300px; min-width: 150px; font-size: 13px; font-weight: 900;color:red; overflow-wrap: break-word;}"
		".author {font-size: 13px; font-weight: 900;}"
		".mail{font-size: 13px; font-weight: 900;}"
		".date{display:inline-block; background-color: white; color: red; padding: 6px 10px; margin: 5px 20px; text-align: center; font-size: 12px; font-weight: 600;}"
		"</style></head><body>";
	string divDateTime = "<div id=\"date\" class=\"date\">Updated:"+divdate+"</div><table>";
	string footer = "</table></body></html>";
	string table = "<tr class='header'><th>ID</th><th>SCAN DATE</th><th>FILE</th><th>MR DATE</th><th>LINE</th><th>DEFECT MESSAGE</th><th>AUTHOR</th><th>MAIL</th>";
	string rowstart = "<tr>";
	string rowend = "</tr>";
	string ahref = "<a href=\"https://gitlab.cern.ch/atlas/athena/tree/";
	auto id_date = currentDT();
	int counter = 1;
	
		
	while(getline(read, line)){
		if(line.length()<=40)
			continue;
			
		if(line.find("<error") == string::npos)
			continue;
			
			string scan_date = GetData("scan_date",line);
			
			string file = GetData("file",line);
				file = file.substr(6,file.length()+1);
				
			string mrdate = GetData("mrdate",line);
			
			string lineN = GetData("line",line);
			
			string msg = GetData("msg",line);
			
			string author = GetData("author",line);
			
			string mail = GetData("mail",line);
			
		table +=rowstart;
		
	//		table += "<th class='status'></th>" ;
			
			table += ("<th class='id_num'>" + id_date + to_string(counter++) + "</th>" );	

			table += "<th class='scan_date'>" + scan_date + "</th>";

			table += "<th class='file'>" + ahref + tagsName + file +"\"" + "</a>" + "athena"+ file + "</th>"; 
			
			table += "<th class='mrdate'>" + mrdate + "</th>";
			
			table += "<th class='line'>" + ahref + tagsName + file + "#L" + lineN +"\"" + "</a>"+ lineN + "</th>";
		
			table += "<th class='msg'>" + msg + "</th>";
		
			table += "<th class='author'>" + author + "</th>";
		
			table += "<th class='mail'>" + mail + "</th>";
	
		table+=rowend;
	}
	cout<< "Converting defects.xml to .html table"<<endl;
	ofstream write("newdefects.html");
	write << header + divDateTime + table + footer;
	
	
	return 0;
}
