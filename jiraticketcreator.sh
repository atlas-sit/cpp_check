#!/bin/bash

chmod +x jiraticketcreator.sh

#creates ldap.sh file for username search
g++ -std=c++11 ldapshellcreator.cpp -o ldapshellcreator.out
./ldapshellcreator.out


chmod a+x ldap.sh
#runs username search script
./ldap.sh

#modifies and adds username to diff.xml
g++ -std=c++11 LdapReader.cpp -o LdapReader.out
./LdapReader.out

#creates data for curl request
g++ -std=c++11 jiradatamaker.cpp -o jiradatamaker.out
./jiradatamaker.out

g++ -std=c++11 eraselastchar.cpp -o eraselastchar.out
./eraselastchar.out

#cookie based authentication for jira website
cern-get-sso-cookie -u https://its.cern.ch/jira/loginCern.jsp -o jira.txt
#curl post request
curl -b jira.txt \
	-X POST  https://its.cern.ch/jira/rest/api/2/issue/bulk \
	--data @jj.txt \
	-H "Content-Type:application/json" \
	-i

