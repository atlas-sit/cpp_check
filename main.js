const fs = require('fs');
const parser = require('xml2json');
const http = require('http');
const exec = require('child_process').exec;




// let args =  process.argv.splice(2);
let args = [];

if( args[0] == undefined ) {
	args[0] = "Defectsold.xml"
}
if( args[1] == undefined ) {
	args[1] = "Defectsnew.xml"
}

// var DATA = new Map();

var ERROR_LENGTH = 0;
class ERROR {

	/*
		returns stringified object elements
	*/
	GetString() {
		return JSON.stringify( this.Get() );
	}


	/*
		returns default object elements 
		!!!IMPORTANT!!! this is what makes difference when compared. 
		!!!MODIFY!!! onli if you want to change comparing categories
	*/
	Get() {
		var el = new Object();
			el.file = this.file;
			el.message = this.message;
			el.type = this.type;
		return el;
	}
	

	/*
		return full class object
	*/
	GetFull() {
		return this;
	}


	/*
		read NNN tags and generate Javascript Object Structure. 
		Later push result in MultiMap's section of filename
	*/
	constructor( tag, DATA ) {
		ERROR_LENGTH++;
		// console.log( "Found: " + ERROR_LENGTH );


		/* 
			declare variables. 
			some default variables are pre-defined, without modification
		*/
		this.file;
		this.message = tag.msg;
		this.line;
		this.type = tag.severity;

		if(tag.location[0] == undefined) {
			this.file = tag.location.file;
			this.line = tag.location.line;
		} else {
			for( var k in tag.location[0]) {
				this.file = tag.location[0]['file'];
				this.line = tag.location[0]['line'];
			}
		}

		var scope = this;


		/*
			check if element exists in multimap
			if exists, than just add to MAP, by the name of the file: 	{ thisfile: [1,2,3, new] }
			if does not, than make array, on name of file in MAP: 		{ thisfile: [ new ] }
		*/	
		 if( DATA[this.file] != undefined ) {
		 	console.log( "EXISTS: " + this.file )
		 	DATA[ this.file ].push( this );

		 } else {
			console.log( "CREATING: " + this.file )
			DATA[ this.file ] = [ this ];

		}
	}
}








/*
	Handle Data.
	DATA Map has all defects in it. 
	this function runs throught all DATA elements and cleans extra Defects by GetString() and Get() pre-written comparemethods
	if you want to change compare methods, modify Get function's return objects! 
*/
function HandleData( DATA ) {
	for(let i in DATA) {

		if( DATA[i].length <= 1 ) {
			continue;
		}

		let start = [DATA[i][0].GetString()];


		/*
			loop througth DATA[i]_th array
		*/
		for( let k in DATA[i] ) {
			let found = false;

			/*
				compare each element to start array
			*/
			for( let z = 0; z<start.length; z++) {
				if( start[z] == DATA[i][k].GetString() ) {
					// console.log( start[z] );
					found = true;
					DATA[i].splice( k, 1 );
				}
			}


			if(!found) {
				start.push( DATA[i][k] );
			}
		}
		// console.log(start)
		// console.log(DATA[i]);

	}
}






/*
	display data from passed MAP	
*/
function DisplayDATA( DATA ) {

	for( var i in DATA ) {
		console.log( "\n\n----\tfile: "+i+"\n" )
		console.log( DATA[i] )
	}

}


/*
	convert javascript object to JSON String
*/
function GetData( DATA ) {
	return JSON.stringify( DATA );
}




























/*
██████╗  ██████╗ ██████╗ ██╗   ██╗    ██████╗  ██████╗ ██████╗ ██╗   ██╗    ██████╗  ██████╗ ██████╗ ██╗   ██╗    ██████╗  ██████╗ ██████╗ ██╗   ██╗ 
██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝ 
██████╔╝██║   ██║██║  ██║ ╚████╔╝     ██████╔╝██║   ██║██║  ██║ ╚████╔╝     ██████╔╝██║   ██║██║  ██║ ╚████╔╝     ██████╔╝██║   ██║██║  ██║ ╚████╔╝  
██╔══██╗██║   ██║██║  ██║  ╚██╔╝      ██╔══██╗██║   ██║██║  ██║  ╚██╔╝      ██╔══██╗██║   ██║██║  ██║  ╚██╔╝      ██╔══██╗██║   ██║██║  ██║  ╚██╔╝   
██████╔╝╚██████╔╝██████╔╝   ██║       ██████╔╝╚██████╔╝██████╔╝   ██║       ██████╔╝╚██████╔╝██████╔╝   ██║       ██████╔╝╚██████╔╝██████╔╝   ██║    
╚═════╝  ╚═════╝ ╚═════╝    ╚═╝       ╚═════╝  ╚═════╝ ╚═════╝    ╚═╝       ╚═════╝  ╚═════╝ ╚═════╝    ╚═╝       ╚═════╝  ╚═════╝ ╚═════╝    ╚═╝    
*/

















/*
						 ██████╗     ██╗         ██████╗             ███████╗    ██╗    ██╗         ███████╗
						██╔═══██╗    ██║         ██╔══██╗            ██╔════╝    ██║    ██║         ██╔════╝
						██║   ██║    ██║         ██║  ██║            █████╗      ██║    ██║         █████╗  
						██║   ██║    ██║         ██║  ██║            ██╔══╝      ██║    ██║         ██╔══╝  
						╚██████╔╝    ███████╗    ██████╔╝            ██║         ██║    ███████╗    ███████╗
						 ╚═════╝     ╚══════╝    ╚═════╝             ╚═╝         ╚═╝    ╚══════╝    ╚══════╝
*/                                                                                   

var finishedLoading = false;



/*
	Parse XML file for old defects
*/
fs.readFile(args[0], (err, data) => {

	var DATA = new Map();

	/*
		convert XML to JSON, and 'change header' ro errors tag
	*/
	var json = parser.toJson( data ); 
	var errors = JSON.parse(json)['results']['errors']['error'];

	/*
		convert every error to ERROR structure
	*/
	for(var i=0; i<errors.length;  i++) { 
		if( errors[i].id.toLowerCase() == "unknownmacro") {
			continue;
		}
		new ERROR( errors[i], DATA );
		
		// console.log(errors[i]);
	}
	/*
		handleData is called after ERRRORs are finished up. this will same elements that exist on same KEY (in same folder);
	*/
	// HandleData( DATA );



	fs.writeFile('./'+args[0].split(".")[0]+'.json', GetData( DATA ), 'utf8', function(err){
		if( finishedLoading ) {
			exec('node compare.js '+'./'+args[1].split(".")[0]+'.json '+'./'+args[0].split(".")[0]+'.json');
		}
		finishedLoading = true;
		// console.log("error: "+err)
	});
});





/*
							███╗   ██╗    ███████╗    ██╗    ██╗            ███████╗    ██╗    ██╗         ███████╗
							████╗  ██║    ██╔════╝    ██║    ██║            ██╔════╝    ██║    ██║         ██╔════╝
							██╔██╗ ██║    █████╗      ██║ █╗ ██║            █████╗      ██║    ██║         █████╗  
							██║╚██╗██║    ██╔══╝      ██║███╗██║            ██╔══╝      ██║    ██║         ██╔══╝  
							██║ ╚████║    ███████╗    ╚███╔███╔╝            ██║         ██║    ███████╗    ███████╗
							╚═╝  ╚═══╝    ╚══════╝     ╚══╝╚══╝             ╚═╝         ╚═╝    ╚══════╝    ╚══════╝
*/




/*
	Parse XML file for new defects
*/
fs.readFile(args[1], (err, data) => {

	var DATA = new Map();

	/*
		convert XML to JSON, and 'change header' ro errors tag
	*/
	var json = parser.toJson( data ); 
	var errors = JSON.parse(json)['results']['errors']['error'];

	/*
		convert every error to ERROR structure
	*/
	for(var i=0; i<errors.length;  i++) { 
		if( errors[i].id.toLowerCase() == "unknownmacro") {
			continue;
		}
		// console.log(errors[i]);
		new ERROR( errors[i], DATA);
		
		// console.log(errors[i]);
	}
	/*
		handleData is called after ERRRORs are finished up. this will same elements that exist on same KEY (in same folder);
	*/
	// HandleData( DATA ); //TODO: modify handledata so it will compare same file defects better. now it unifies different errors?



	fs.writeFile('./'+args[1].split(".")[0]+'.json', GetData( DATA ), 'utf8', function(err){
		if( finishedLoading ) {
			exec('node compare.js '+'./'+args[1].split(".")[0]+'.json '+'./'+args[0].split(".")[0]+'.json');
		}
		finishedLoading = true;
		// console.log("error: "+err)
	});
});












