const fs = require('fs');
const parser = require('xml2json');
//const oldDefect = JSON.parse(fs.readFileSync('Defectsold.json', 'utf-8')) //args[0]
//const newDefect = JSON.parse(fs.readFileSync('Defectsnew.json', 'utf-8')) //args[1]
let args =  process.argv.splice(2);


if( args[0] == undefined ) {
	args[0] = "Defectsold.json"
}
if( args[1] == undefined ) {
	args[1] = "Defectsnew.json"
}




class STRUCT {

	/*
		read file and store EVERYTHING. new or old defects
		for overaldefects
	*/
	GetFullDefects() {
		/*
			this method will run in both, new and old defects, just to put everything together, and store them for future comparison
			this method will store only UNIVERSAL errors, so we wont have 2 same data stored in file, because they are taking extra space and time
		*/
		var count = 0;
		var storage = [];


		/*
			running throught old defects
		*/
		for( var i in this.old) {
			let elem = this.old[ i ];

			for(var i=0; i<elem.length; i++) {

				let current = elem[i];
				if( storage.indexOf( [current] ) == -1 )
					storage.push( [current] );
				
			}

		}

		/*
			running throught new defects
		*/
		for( var i in this.new) {
			let elem = this.new[ i ];

			for(var i=0; i<elem.length; i++) {

				let current = elem[i];
				if( storage.indexOf( [current] ) == -1 )
					storage.push( [current] );
				
			}

		}

		/*
			running old and new defects
		*/
		return storage;
	}

	/*
		read file and store this data in MAP where all old data is stored
	*/
	GetOldDefects() {

		/*
			this is reversed method of GetNewDefects. just switched dold dnew and vr i in this.new
		*/
		var storage = [];
		for( var i in this.old) {

			var dold = this.new[ i ];
			var dnew = this.old[ i ]; //get new defect file in old defect file.



			/*
				check 1: any defect on file
			*/
			
			if( dold === undefined ) {
			//	console.log(dold);
				storage.push( dnew );
				continue;
			}


			for( var k = 0; k < dnew.length; k++) {
				var found = false;
				for( var z = 0; z < dold.length; z++) {

					/*
						check 3: if message is different
					*/
					if( dnew[k].message == dold[z].message) {
						/*
							enable for line difference
						*/
					//	if(dnew[k].line == dold[z].line) {found = true; break; } 
						found = true;
						break;
					}

				}
				
				if(!found) {
					/*
						enable for extra precision?
					*/
				//	if( storage.indexOf(dnew[k]) > -1 ) { continue; } 
					storage.push(dnew[k]);
					
					
				}

			}

		}

		return storage;
		
	}



	/*
		this is reversed method of GetNewDefects. just switched dold dnew and vr i in this.new
	*/
	GetNewDefects() {
		var storage = [];
		for( var i in this.new) {

			var dnew = this.new[ i ];
			var dold = this.old[ i ]; //get new defect file in old defect file.

			/*
				check 1: any defect on file
			*/
			if( dold == undefined ) {
				storage.push( dnew );
				continue;
			}

			// console.log( this.new[i] )

			for( var k = 0; k < dnew.length; k++) {
				var found = false;
				for( var z = 0; z < dold.length; z++) {

					/*
						check 3: if message is different
					*/
					if( dnew[k].message == dold[z].message) {
						/*
							enable for line difference
						*/
						// if(dnew[k].line == dold[z].line) {found = true; break; } 
						found = true;
						break;
					}

				}
				
				if(!found) {
					/*
						enable for extra precision?
					*/
					// if( storage.indexOf(dnew[k]) > -1 ) { continue; } 
					storage.push(dnew[k]);
				//	console.log(dnew[k]);
				//	console.log(JSON.stringify(dnew[k]));
				
					
				}

			}

		}

		/*
			return stored data ... wow so ... helpful
		*/
		// var strfied = JSON.stringify(storage);

		// fs.writeFile('./results.json', strfied, 'utf8', function(err){
		// 	console.log("error: "+err)
		//	});
		return storage;

	}

	/*
		just declaring 2 JSON type files.
	*/
	constructor(oldjson, newjson) {
		this.old = oldjson;
		this.new = newjson;
	}
}

/*
<error 
msg="Syntax Error: AST broken, &apos;for&apos; doesn&apos;t have two operands." 
file="athena/InnerDetector/InDetRecTools/SiCombinatorialTrackFinderTool_xk/src/SiCombinatorialTrackFinder_xk.cxx" 
line="686"
/>

<error 
id="internalAstError" 
msg="Syntax Error: AST broken, &apos;for&apos; doesn&apos;t have two operands." 
verbose="Syntax Error: AST broken, &apos;for&apos; doesn&apos;t have two operands." 
file="athena/InnerDetector/InDetRecTools/SiCombinatorialTrackFinderTool_xk/src/SiCombinatorialTrackFinder_xk.cxx" 
line="686"
/>

*/




/*
	Convert Stored data to XML file
*/


function DataToXML( data, name = "results" ) {
	var str = "";

	for( var i in data) {
		
		for( var k in data[i]) {
		
		//	console.log(data[i][k])
			var line = '<error msg=\"' + data[i][k].message + '\" file=\"'+ data[i][k].file + '\" line=\"'+ data[i][k].line + '\" />';
				line.replace("\\\\", "/");

			str += line +'\n';


		}
	}



	/*
		make new file
	*/
	fs.writeFile('./'+name+".xml", str, 'utf8', function(err){
		console.log("error: "+err)
	});

}











/*
██████╗  ██████╗ ██████╗ ██╗   ██╗    ██████╗  ██████╗ ██████╗ ██╗   ██╗    ██████╗  ██████╗ ██████╗ ██╗   ██╗    ██████╗  ██████╗ ██████╗ ██╗   ██╗ 
██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝    ██╔══██╗██╔═══██╗██╔══██╗╚██╗ ██╔╝ 
██████╔╝██║   ██║██║  ██║ ╚████╔╝     ██████╔╝██║   ██║██║  ██║ ╚████╔╝     ██████╔╝██║   ██║██║  ██║ ╚████╔╝     ██████╔╝██║   ██║██║  ██║ ╚████╔╝  
██╔══██╗██║   ██║██║  ██║  ╚██╔╝      ██╔══██╗██║   ██║██║  ██║  ╚██╔╝      ██╔══██╗██║   ██║██║  ██║  ╚██╔╝      ██╔══██╗██║   ██║██║  ██║  ╚██╔╝   
██████╔╝╚██████╔╝██████╔╝   ██║       ██████╔╝╚██████╔╝██████╔╝   ██║       ██████╔╝╚██████╔╝██████╔╝   ██║       ██████╔╝╚██████╔╝██████╔╝   ██║    
╚═════╝  ╚═════╝ ╚═════╝    ╚═╝       ╚═════╝  ╚═════╝ ╚═════╝    ╚═╝       ╚═════╝  ╚═════╝ ╚═════╝    ╚═╝       ╚═════╝  ╚═════╝ ╚═════╝    ╚═╝    
*/




var file_old;
var file_new;

fs.readFile(args[0], (err, data) => {
	file_old = JSON.parse( data )
fs.readFile(args[1], (err, data) => {
	file_new = JSON.parse( data )

	function Main() {


		var DATA = new STRUCT(file_old, file_new);
		var defects_new = DATA.GetNewDefects();
		var defects_old = DATA.GetOldDefects();
	//	var defects_total = DATA.GetFullDefects();

		// console.log( defects_total );

	//	DataToXML( defects_total, "overalldefects" );
		// DataToXML( defects_new, "current_fixed_diff" );
		// DataToXML( defects_old );

		DataToXML( defects_old, "current_fixed_diff" );
		DataToXML( defects_new );







	}
	Main();

});
});







