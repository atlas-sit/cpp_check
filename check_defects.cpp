#include <iostream>
#include <fstream>
#include <string>




using namespace std;

int main(){

	std::ifstream file("results.xml");

	if(!file){

		cout << "results.xml not found"<<endl;
	    // file is not open
	    return 1;
	}

	if ( file.peek() == std::ifstream::traits_type::eof()){

		cout<<"We don't have any Defects today"<<endl;
		   // Empty File
		return 2;
	}

//file is open and isn't empty
//cout<<"Continue automation"<<endl;

	string run_jira = "./jiraticketcreator.sh";
	std::system(run_jira.c_str());


	return 0;
}