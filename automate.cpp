#include <iostream>
#include <sys/stat.h> 
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <array>
#include <memory>
#include "overalldefects.cpp"
#include "overall_fixed_diff.cpp"



struct stat sb;

using namespace std;

time_t my_time = time(NULL); 

string ff2;

struct Gitclone
{
	Gitclone()
	{
		pulling();
		scan();
	}
	struct stat info;

	bool is_directory( std::string pathname ) 
	{

		if( stat( pathname.c_str(), &info ) != 0 )
		    return 0;

		else if( info.st_mode & S_IFDIR )
		    return 1;

		else
		    return 1;
	}

	void pulling(){
		string pathname= "athena";
		if (is_directory(pathname))
		{	
			cout<<" ======= Phase 1: ======="<<endl;
			cout<<" ======= PULLING ATHENA REPOSITORY ======= "<<endl;
			string gitpull= "git pull https://gitlab.cern.ch/atlas/athena.git";
			
			chdir(pathname.c_str());
			std::system(gitpull.c_str());
		}
	
		else 
		{
			std::cout<<" ======= Phase 1: ======= "<<endl;
			std::cout<<"====== CLONING ATHENA REPOSITORY ====== ";
			std::string gitclone = "git clone https://gitlab.cern.ch/atlas/athena.git";
			
			std::system(gitclone.c_str());
			
		}	
}
	void changename()
	{
			if (stat("Defectsprev.xml", &sb) == 0 && S_ISREG (sb.st_mode))
		{
		 	remove("Defectsprev.xml");
		
		}
			if (stat("Defectsold.xml", &sb) == 0 && S_ISREG (sb.st_mode))
		{
		 	
		 	rename("Defectsold.xml", "Defectsprev.xml");
		 
		
		}
		if (stat("Defectsnew.xml", &sb) == 0 && S_ISREG (sb.st_mode))
		{
		 	cout<<" ====== Defectnew File Already Exists ====== "<<endl;
		 	rename("Defectsnew.xml", "Defectsold.xml");

		
		}
		else 
		{
			cout<<" ====== Defectnew File Does Not Exist ====== "<<endl;

		}
	}

	
	void scan(){																			
		string changedirback = "..";
		chdir(changedirback.c_str());	
		changename();
		cout<<"\n ======= Phase 2: ======= "<<endl;
		cout<<" ====== Starting Cppcheck Scan ====== "<<endl;
		string scan= "cppcheck --std=c++17 --enable=warning,portability,performance --suppress=useStlAlgorithm --suppress=assignmentInAssert:*_test.cxx --suppress=assertWithSideEffect:*_test.cxx --suppress=accessMoved --suppress=uninitMemberVar --suppress=useInitializationList --inline-suppr --xml -D__CPPCHECK__ -DATOMIC_POINTER_LOCK_FREE=2 --xml athena 2> Defectsnew.xml" ;
		std::system(scan.c_str());
	} 

};

string exec(const string cmd) { 
	array <char, 128> buffer;
	string result;
	unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
		if (!pipe) {
		    throw std::runtime_error("popen() failed!");
		}
		while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		    result += buffer.data();
		}
	return result;
}

string read(string filename){
	ifstream file(filename);

	string line;
	string data;

	while( getline( file, line ) ) {
		data+=line+"\n";
	}

	return data;

}

void write(string filename, string data){
	ofstream ofile(filename);
	ofile << data;
}

const std::string DateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m-%Y", &tstruct);

    return buf;
}

 void defect_info(string openfile, string writefile){

	auto scan_date = DateTime();
	
	ifstream read(openfile);
	string line;
	string data;
	string datastring;
	string path = "file=\"athena";


	while(getline (read, line)){
		auto foundPosition = line.find(path);
		if(foundPosition == string::npos )
		{
			datastring += line +"\n";
		}
		else{
			string str;
			for(auto it=foundPosition+path.length()+1; ; ++it) {
				if(line[it] == '"' && line[it+1] == ' ' || line[it] == '>')
					break;
				str+= line[it];
			}

			chdir("athena");
			string mrdate = exec("git log -n 1 --pretty=format:%ad --date=short --  "+ str);
			string name = exec("git log -1 --pretty=format:%an --  "+ str);
			string mail = exec("git log -1 --pretty=format:%ae --  "+ str);
			
			string my_mrdate;
			string entered_month;
			string entered_year;
			string entered_day;

				entered_month = mrdate.substr(5,2);
				entered_day = mrdate.substr(8,2);
				entered_year = mrdate.substr(0,4);

				my_mrdate = entered_day + "-" + entered_month + "-" + entered_year;


			
			chdir("..");
			
			if(line.find("<error ") != string::npos ){
				
				line.insert( line.length()-2," scan_date=\""+scan_date+"\" ");
				line.insert( line.length()-2," mrdate=\"" +my_mrdate+ "\" ");	
				line.insert( line.length()-2," author=\"" +name+ "\" ");
				line.insert( line.length()-2," mail=\"" +mail+ "\" ");
				
				datastring += line +"\n";	
			}

		}
	}

	data = datastring;
	write(writefile, data);
}

int main()
{
	
	Gitclone gitclone;
	
	cout<<"\n ======= Phase 3: ======= "<<endl;
	cout<<" ======= Created results.xml file ======= "<<endl;

		//vadarebt mimdinare da wina kviris skanirebis failebs
		exec("scl enable rh-nodejs12 'node main.js'");
	//	exec("node main.js");
		exec("scl enable rh-nodejs12 'node compare.js'");
	//	exec("node compare.js");
	
	cout<<" ======= Done ======= "<<endl;
	

	
	cout<<"\n ======= Phase 4: ======= "<<endl;
	cout<<" ======= Getting Authors, Mails, MR Date ======= "<<endl;

		defect_info("results.xml","results.xml");
		defect_info("current_fixed_diff.xml","current_fixed_diff.xml");


	cout<<" ======= Done ======= "<<endl;



	overalldefects();
	
	overall_fixed_diff();


return 0;
}
