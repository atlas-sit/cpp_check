#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <iterator>

using namespace std;

map <string,string> data;

string getFullName(string line) {
	
	string str = "";

	for(int i = line.find("displayName") + 12; i < line.length(); i++) {
		
		if( line[i] == ')' )
			break;

		str+=line[i];

	}
	return str;

}

void getTag(string tag, string filename) {

	ifstream file(filename);
	string line;
	string found = "";
	string name = "";
	

	while( getline(file, line) ) {

		if( line.find("displayName") != string::npos ){
			name = getFullName(line);
		}

		if(line[0] == '#')
			continue;

		int index = line.find(tag);

		if(index<0)
			continue;

		bool isFirst = true;
		bool begun = false;
		for(int i= tag.length()+2+index; i<line.length(); i++) {

			if(line[i] == ',' || line[i] == ' ' || line[i] == '.' || line[i] == ';') {

				cout << name << " - " << found << endl;
				data[name] = found;
				found = "";
				
			}
			begun = true;
			found += line[i];
		}
		data[name] = found;
		found = "";
	}
}

string GetData(string tag, string line){
	int size = tag.length()+2;
	int pos = line.find(tag+"=\"");
	string data = "";
	for( int i=pos+size; ; i++){

		if(line[i]== '=' && line[i+1]== '"')
			return "";
			
		if(line[i]== '"')
			return data;
			
		data += line[i];	
	}
	return "";
}

int main() {
	
	
	map <string,string>::iterator it;
	getTag("sAMAccountName", "ldap.txt");
	ifstream read("results.xml");
	string line;
	string datastring;
	string username;
	
					
	while(getline(read, line)){
		if(line.length()<=40)
			continue;
			
		if(line.find("<error ") != string::npos ){
			
			string author = GetData("author",line);
	
					line.insert( line.length()-2," username=\"" + data[author] + "\" ");
					datastring += line +"\n";

		}	//end if
		
	}	//end while

	ofstream write("results.xml");
	write << datastring;
	

	
}
