#include <iostream>
#include <sys/stat.h> 
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <array>
#include <memory>

using namespace std;

string GetData(string tag, string line){
	int size = tag.length()+2;
	int pos = line.find(tag+"=\"");
	string data = "";
	for( int i=pos+size; ; i++){

		if(line[i]== '=' && line[i+1]== '"')
			return "";
			
		if(line[i]== '"')
			return data;
			
		data += line[i];	
	}
	return "";
}

int main(){ //

	struct stat sb;
	if (stat("ldap.txt", &sb) == 0 && S_ISREG (sb.st_mode)){
		cout<<"ldap.txt file exists"<<endl;
		cout<<"removing ldap.txt file"<<endl;
		 	remove("ldap.txt");
	}
		else{
			cout<<"ldap.txt file doesn't exists"<<endl;
		}


	ifstream read("results.xml");
	string line;
	string ldap = "";
	
		
	while(getline(read, line)){
		if(line.length()<=40)
			continue;
			
		if(line.find("<error") == string::npos)
			continue;
			
			string author = GetData("author",line);
		
		ldap += "/usr/bin/ldapsearch -x -h xldap.cern.ch \\\n";
		ldap += "\t-b 'OU=Users,OU=Organic Units,DC=cern,DC=ch' '(&(objectClass=user) (displayName=" + author + "))'\\\n";
		ldap += "\tsAMAccountName \\\n";
		ldap += "\t>>ldap.txt\n\n";

	}
	
	ofstream write("ldap.sh");
	write << ldap;
	
	
	return 0;
}
