#include <iostream>
#include <sys/stat.h> 
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdio>
#include <array>
#include <memory>

using namespace std;

string GetData(string tag, string line){
	int size = tag.length()+2;
	int pos = line.find(tag+"=\"");
	string data = "";
	for( int i=pos+size; ; i++){

		if(line[i]== '=' && line[i+1]== '"')
			return "";
			
		if(line[i]== '"')
			return data;
			
		data += line[i];	
	}
	return "";
}
const std::string currDT() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m-%Y", &tstruct);

    return buf;
}

string read(string filename){
	ifstream file(filename);
	string line;
	string data;

	while( getline( file, line ) ) {
		data+=line+"\n";
	}
	return data;
}

void write(string filename, string data){
	ofstream ofile(filename);
	ofile << data;
}

void convertor(string openfile, string writefile){

	fstream read(openfile);
	string line;
	string data;
	auto divdate = currDT();
	
	const char *header = 
		"<!DOCTYPE html><html><head><title>Defects List</title>"
		"<meta http-equiv=\"Cache-Control\" content=\"no-store\">"
		"<meta http-equiv=\"Pragma\" content=\"no-cache\">"
		"<meta http-equiv=\"Expires\" content=\"0\">"
		"<style>"
		"table {border: 2px dotted black; width: 100%; height: 100%;} "
		"th {border: 1px solid black; padding: 5px;} "
		"tr {min-height: 150px;}"
		".header {font-size: 17px; font-weight: 900;border: 0px; border-bottom: 10px solid black;} "
		".file {max-width: 400px; min-width: 150px; font-size: 13px; overflow-wrap: break-word;} "
		".id_num {font-size: 13px;font-weight: 900;}"
		".scan_date {font-size: 13px;font-weight: 900;}"
		".mrdate {font-size: 13px;font-weight: 900;}"
		".line {font-size: 13px;font-weight: 900;}"
		".msg {max-width: 300px; min-width: 150px; font-size: 13px; font-weight: 900;color:red; overflow-wrap: break-word;}"
		".author {font-size: 13px; font-weight: 900;}"
		".mail{font-size: 13px; font-weight: 900;}"
		".date{display:inline-block; background-color: white; color: red; padding: 6px 10px; margin: 5px 20px; text-align: center; font-size: 12px; font-weight: 600;}"
		"</style></head><body>";

/*
	//odd row color changer

	tr:nth-child(even) {background-color: #d4d6ce}
	
*/

	string divDateTime = "<div id=\"date\" class=\"date\">Updated:"+divdate+"</div><table>";
	string footer = "</table></body></html>";
	string table = "<tr class='header'><th>ID</th><th>SCAN DATE</th><th>FILE</th><th>MR DATE</th><th>LINE</th><th>DEFECT MESSAGE</th><th>AUTHOR</th><th>MAIL</th>";
	string rowstart = "<tr>";
	string rowend = "</tr>";
	int counter = 1;
		
	while(getline(read, line)){
		if(line.length()<=40)
			continue;
			
		if(line.find("<error") == string::npos)
			continue;


		string scan_date = GetData("scan_date",line);
			
		string file = GetData("file",line);
			
		string mrdate = GetData("mrdate",line);
		
		string lineN = GetData("line",line);
		
		string msg = GetData("msg",line);
		
		string author = GetData("author",line);
		
		string mail = GetData("mail",line);
			
		table +=rowstart;
			
			table += ("<th class='id_num'>"+to_string(counter++)+"</th>" );	

			table += "<th class='scan_date'>" + scan_date + "</th>";

			table += "<th class='file'>" + file + "</th>";

			table += "<th class='mrdate'>" + mrdate + "</th>";
			
			table += "<th class='line'>" + lineN + "</th>";
		
			table += "<th class='msg'>" + msg + "</th>";
		
			table += "<th class='author'>" + author + "</th>";
		
			table += "<th class='mail'>" + mail + "</th>";
	
		table+=rowend;
	}

	data = header + divDateTime + table + footer;

	write(writefile, data);
}

int main(){
	
	cout<<"====== Converting .xml files to .html ======"<<endl;
	
	convertor("overalldefects.xml", "overalldefects.html");
	convertor("current_fixed_diff.xml", "current_fixed_diff.html");
	convertor("overall_fixed_diff.xml", "overall_fixed_diff.html");
	
	return 0;
}
